#This file is part of Sivigik.
#
#Foobar is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Foobar is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
"""
Django settings for Sivigik project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True#False

if DEBUG:
    CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}

COMPRESS_ENABLED = True

TEMPLATE_DEBUG = True
ALLOWED_HOSTS = ('*')
INTERNAL_IPS = ('127.0.0.1')

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'templates'),
)
# TEMPLATE_LOADERS = (
# ('django_mobile.loader.CachedLoader', (
# 'django_mobile.loader.Loader',
# 'django.template.loaders.filesystem.Loader',
# 'django.template.loaders.app_directories.Loader',
# )),
# )

TEMPLATE_CONTEXT_PROCESSORS = (
"django.contrib.auth.context_processors.auth",
"django.core.context_processors.debug",
"django.core.context_processors.i18n",
"django.core.context_processors.media",
# "django_mobile.context_processors.flavour",
# "django_mobile.context_processors.is_mobile",
)

ADMINS = (
    ('user', 'mail'),
)

IS_MAINTENANCE = False
SECRET_KEY = 'secret!'

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sitemaps',
    #'debug_toolbar',
    'compressor',
	'home',
	'article',
    # 'author',
    'utils',
    'maintenance',
    'search',
    'staticContent',
    #'south',
    #'poll',
    'member',
)

COMPRESS_PRECOMPILERS = (
    ('text/scss', 'sass --scss {infile} {outfile}'),
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # 'django_mobile.middleware.MobileDetectionMiddleware',
    # 'django_mobile.middleware.SetFlavourMiddleware',

)

STATICFILES_FINDERS = (
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
    'compressor.finders.CompressorFinder',
    )

ROOT_URLCONF = 'Sivigik.urls'

WSGI_APPLICATION = 'Sivigik.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'sivigik_base',
        'USER': 'sivigik',
        'PASSWORD' : 'gHAz9uepo56',
        'HOST' : '127.0.0.1',
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'fr-FR'

TIME_ZONE = 'Europe/Paris'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/
STATIC_ROOT = BASE_DIR + '/public/static/'
STATIC_URL = '/static/'

MEDIA_ROOT = BASE_DIR + '/public/media/'
MEDIA_URL = '/media/'

COMPRESS_ROOT = STATIC_ROOT
